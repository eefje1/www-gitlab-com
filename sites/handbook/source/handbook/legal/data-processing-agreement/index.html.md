---
layout: handbook-page-toc
title: "GitLab Data Processing Agreement and Standard Contractual Clauses"
description: "This agreement ..."
---

<a href="https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Customer_DPA__5.23.22_.pdf">GitLab Data Processing Addendum</a>

<a href="https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Exhibit_B__Standard_Contractual_Clauses_5_25_22.pdf"> Exhibit B - Standard Contractual Clauses</a>
